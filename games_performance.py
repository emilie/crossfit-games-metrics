
import requests
import json
import pandas as pd
from pandas.io.json import json_normalize
# division = '1'  # men = 1, women = 2

def fetch_data(years, divisions):
    params = {
        "year": years,
        "division": divisions
    }
    url = """https://games.crossfit.com/competitions/api/v1/competitions/games/%(year)s/leaderboards?division=%(division)s&sort=0&page=1ga"""
    response = requests.get((url % params))
    data = response.json()
    data = json.dumps(data)
    parsed = json.loads(data)
    return(parsed)


def get_final_scores(years, divisions, genders):
    params = {
        "year": years,
        "division": divisions,
        "gender": genders
    }

    raw = fetch_data(years, divisions)

    rankings = json_normalize(raw['leaderboardRows'])
    filename = ("crossfit_performance_%(gender)s_%(year)s.csv" % params)
    rankings.to_csv(filename, sep=',', encoding='utf-8')
    print("Go look at your file now! Its name is " + filename)


def get_event_data(years, divisions, genders, limit=30):
    params = {
        "year": years,
        "division": divisions,
        "gender": genders
    }

    raw = fetch_data(years, divisions)

    scores = pd.DataFrame()

    rankings = json_normalize(raw['leaderboardRows'])

    for i in range(limit):
        df = json_normalize(raw['leaderboardRows'][i]['scores'])
        athlete_name = raw['leaderboardRows'][i]['entrant']['competitorName']
        df['athlete'] = athlete_name
        scores = scores.append(df)

    filename = ("crossfit_event_performance_%(gender)s_%(year)s.csv" % params)
    scores.to_csv(filename, sep=',', encoding='utf-8')
    print("Go look at your file now! Its name is " + filename)
