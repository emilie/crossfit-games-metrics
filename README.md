# Crossfit Games Metrics Scraper

A quick python3 scraper that gets data from http://games.crossfit.com/games

Since watching Camille Leblanc-Bazinet win Event 2 at the 2018 Games this year, I’ve had a silly question rolling around in the back of my head: How many Event wins come from people not in the final heat? Are they ever not in the second-to-last heat?

I’m not pondering the problems of the universe here, and lucky for me there’s an easy solution to this: look at the data. When I looked at the games website, though, I was shocked at how not-friendly-for-analysis it is. I decided to make it friendly for analysis by building a python scraper that takes the JSON data from the API and the normalizes it into two tables: overall performance and individual event performance.

There are some .csv files included above that can be downloaded. Enjoy!

If you have data-driven questions about the Games results, you can create an Issue in the [Issue Tracker](https://gitlab.com/emilie/crossfit-games-metrics/issues). Maybe I’ll get to it.

### Usage

`get_final_scores(years, divisions, genders, limit)` outputs the overall performance. 

`get_event_data(years, divisions, genders, limit)` outputs the event-by-event performance for the athletes. 

* `years` - the year of competition; the 2018 games is `2018`.
* `divisions` - there is a key down below. These changed for the 2015 games. If you're interested in using this to pull data from prior to then, you should look up the division info on the website. 
* `genders` - this is not used in analysis and simply for reference in the final file name.
* `limit` - Do you want the first 10? the first 20? If you list more than there are, it will fail.

Division Info (2015-present)
------
| 1 | Men |  
| 2 | Women |  
| 3 | Team |  
| 4 | Men (35-39) |  
| 5 | Women (35-39) |  
| 6 | Men (40-44) |  
| 7 | Women (40-44) |  
| 8 | Men (45-49) |  
| 9 | Women (45-49) |  
| 10 | Men (50-54) |  
| 11 | Women (50-54) |  
| 12 | Men (55-59) |  
| 13 | Women (55-59) |  
| 14 | Men (60+) |  
| 15 | Women (60+) |  
| 16 | Boys (16-17) |  
| 17 | Girls (16-17) |  
| 18 | Boys (14-15) |  
| 19 | Girls (14-15) |  
