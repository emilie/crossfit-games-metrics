import requests
import json
import pandas as pd
from pandas.io.json import json_normalize
from datetime import datetime

today = datetime.now()
year = today.date().year

years = list(range(2018, (year+1)))
 #### 2018 and future

divisions = [1, 2]

for year in years:
    for division in divisions:
        params = {
            "year": year,
            "division": division
        }
        url = """https://games.crossfit.com/competitions/api/v1/competitions/open/%(year)s/leaderboards?division=%(division)s&sort=0&page=1ga"""
        response = requests.get(url % params)
        data = response.json()
        data = json.dumps(data)
        parsed = json.loads(data)
        raw = json_normalize(parsed['leaderboardRows'])
        filename = ("open_data/open_performance_%(division)s_%(year)s.csv" % params)
        raw.to_csv(filename, sep=',', encoding='utf-8')