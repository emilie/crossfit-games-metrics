from bs4 import BeautifulSoup
import requests
import requests
import json
import csv
import pandas as pd
from pandas.io.json import json_normalize

years = [11, 12, 13, 14, 15, 16]
divisions = [1, 2]

for division in divisions:
    for year in years:
        params = {
                "year": year,
                "division": division
                }
        url = "https://games.crossfit.com/scores/leaderboard.php?stage=0&sort=0&division=%(division)s&region=0&regional=5&numberperpage=60&page=0&competition=0&frontpage=0&expanded=1&full=1&year=%(year)s&showtoggles=0&hidedropdowns=1&showathleteac=1&athletename=&fittest=1&fitSelect=0&scaled=0&occupation=0"
        data = pd.read_html(url % params)
        data = pd.DataFrame(data[0])
        data.to_csv('temp.csv', sep=',', encoding='utf-8')
        df = pd.read_csv('temp.csv', header=1)
        df['open_year'] = "%(year)s"
        filename = ("open_data/open_performance_%(division)s_%(year)s.csv" % params)
        df.to_csv(filename, sep=',', encoding='utf-8')

### 2017 DATA
divisions = [1, 2]
for division in divisions:
    params = {
        "division": division
        }
    url = """https://games.crossfit.com/competitions/api/v1/competitions/open/2017/leaderboards?division=%(division)s&sort=0&page=1ga"""
    response = requests.get(url % params)
    data = response.json()
    data = json.dumps(data)
    parsed = json.loads(data)
    raw = json_normalize(parsed['athletes'])
    filename = ("open_data/open_performance_%(division)s_17.csv" % params)
    raw.to_csv(filename, sep=',', encoding='utf-8')

